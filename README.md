# MapSelectorAPI


## Description
The API receives rcon details and the choice the players made from the game server, then sends the SwitchMap rcon command back to your server. 


## Installation
To setup, copy the SwitchMap.py script to the server your game server is hosted from, and run it in the background 1 of 2 ways:
 - cd to the right directory and run 
 ```
 nohup python3 SwitchMap.py &
 ``` 
 This will run leave it running when you close the terminal, but any time your server crashes it will have to be rerun.
 - The other method is to set it up as a systemd service: This allows you to have the script automatically run when your server boots. a guide on setting this up can be found [here](https://medium.com/codex/setup-a-python-script-as-a-service-through-systemctl-systemd-f0cc55a42267).

After you've got the API running, send the command (case sensitive) 'UseLocal' to your server via rcon (must be done while the mod is running) to switch it to using your local API. The 'UseServer' command will switch it back to using my server.


