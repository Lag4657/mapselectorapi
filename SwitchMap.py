# SwitchMap.py
# Accepts a json body in the format: {"IP":..., "Port":..., "Password":..., "Choice":...,}

from flask import Flask, request, jsonify
import asyncio
from pavlov import PavlovRCON
import requests
from config import webhook
import traceback

app = Flask(__name__)

async def switchmap(content: dict) -> dict:
    rcon = PavlovRCON(content['IP'], int(content['Port']), content['Password'])
    data = await rcon.send(f'SwitchMap {content["Choice"]}')
    rcon.close()
    return data

@app.route('/switchmap', methods=['POST'])
def switchmapCalled():
    try:
        content:dict = request.json
        log = content.copy()
        log.pop('Password')
        with open('api.log','a') as x:
            x.write('\n/switchmap'+str(log))
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        data = loop.run_until_complete(switchmap(content))
        data.update({"Choice":content['Choice']})

        message = f'{content["IP"]}: SwitchMap {content["Choice"]}'
        data = {'content':message}
        requests.post(webhook,json=data)
    except:
        with open('api.log','a') as x:
            x.write(traceback.format_exc())
        


    return jsonify(data)

@app.route('/test')
def test():
    with open('api.log','a') as x:
        x.write('\n/test')
    return {'yeah we': 'working'}

if __name__ == '__main__':
    app.run()